import { Accessor, Setter } from "solid-js"

export interface AppInfo {
    page: Accessor<"categories" | "todos" | "generators">
    setPage: Setter<"categories" | "todos" | "generators">
    category: Accessor<string | null>
    setCategory: Setter<string | null>
    categories: Accessor<string[]>
    setCategories: Setter<string[]>
    todos: Accessor<Todo[]>
    setTodos: Setter<Todo[]>
    generators: Accessor<Generator[]>
    setGenerators: Setter<Generator[]>
}

enum Priority {
    Low,
    Normal,
    High,
}

export interface Todo {
    title: string
    description: string
    at: Date | null
    subtasks: Subtask[]
    priority: Priority
    category: string | null
}

export interface Subtask {
    title: string
    complete: boolean
}

export interface Template {
    title: string
    description: string
    at: Date | null // Only used for the time component
    subtasks: Subtask[]
    priority: Priority
}

export interface Generator {
    templates: Template[]
    generate: Weekly | Daily
    lastGenerated: Date
    category: string | null
}

function isSameDay(a: Date, b: Date): boolean {
    if (Math.abs(a.getTime() - b.getTime()) > 1000 * 60 * 60 * 24) return false

    return a.getDate() === b.getDate()
}

// Source: https://stackoverflow.com/questions/75352408/trying-to-check-if-the-2-dates-are-in-the-same-week

function getWeekNum(date: Date): number {
    const janFirst = new Date(date.getFullYear(), 0, 1)
    // Source: https://stackoverflow.com/a/27125580/3307678
    return Math.ceil(
        ((date.getTime() - janFirst.getTime()) / 86400000 +
            janFirst.getDay() +
            1) /
            7,
    )
}

function isSameWeek(dateA: Date, dateB: Date): boolean {
    return getWeekNum(dateA) === getWeekNum(dateB)
}

function maybeGenerate(generator: Generator): Todo[] {
    let now = new Date()
    let lastGenerated = generator.lastGenerated

    let ret = []

    if ("daysInAdvance" in generator.generate) {
        let generateFrom = structuredClone(generator.lastGenerated)

        let minStart = new Date()
        minStart.setDate(minStart.getDate() - generator.generate.daysInAdvance)

        // Don't generate todos due in the past
        if (!isSameDay(generateFrom, minStart) && generateFrom < minStart) {
            generateFrom = minStart
        }

        while (!isSameDay(now, generateFrom)) {
            generateFrom.setDate(generateFrom.getDate() + 1)

            ret.push(...generateFromDate(generator, generateFrom))
        }
    } else if (
        "weeksInAdvance" in generator.generate &&
        !isSameWeek(now, lastGenerated)
    ) {
        let generateFrom = structuredClone(generator.lastGenerated)

        let minStart = new Date()
        minStart.setDate(
            minStart.getDate() - generator.generate.weeksInAdvance * 7,
        )

        // Don't generate todos due in the past
        if (!isSameDay(generateFrom, minStart) && generateFrom < minStart) {
            generateFrom = minStart
        }

        while (!isSameWeek(now, generateFrom)) {
            generateFrom.setDate(
                generateFrom.getDate() + 7 - generateFrom.getDay(),
            )
        }
    }

    generator.lastGenerated = now

    return ret
}

function generateFromDate(generator: Generator, generateFrom: Date): Todo[] {
    let ret = []

    if ("daysInAdvance" in generator.generate) {
        let generateAt = structuredClone(generateFrom)
        generateAt.setDate(
            generateAt.getDate() + generator.generate.daysInAdvance,
        )

        ret = generateAtDate(
            generator.templates,
            generator.category,
            generateAt,
        )
    } else if ("weeksInAdvance" in generator.generate) {
        let generateAt = structuredClone(generateFrom)
        generateAt.setDate(
            generateAt.getDate() + generator.generate.weeksInAdvance * 7,
        )

        let days = generator.generate.days

        for (let i = 0; i < 7; i++) {
            if (days[i]) {
                generateAt.setDate(generateAt.getDate() + i)

                ret.push(
                    ...generateAtDate(
                        generator.templates,
                        generator.category,
                        generateAt,
                    ),
                )

                generateAt.setDate(generateAt.getDate() - i)
            }
        }
    }

    return ret
}

function generateAtDate(
    todos: Template[],
    category: string | null,
    date: Date,
): Todo[] {
    let ret = []

    for (const todo of todos) {
        let newTodo = structuredClone(todo) as Todo
        newTodo.category = category

        if (newTodo.at !== null) {
            newTodo.at.setFullYear(date.getFullYear())
            newTodo.at.setMonth(date.getMonth())
            newTodo.at.setDate(date.getDate())
        }

        ret.push(newTodo)
    }

    return ret
}

interface Daily {
    daysInAdvance: number
}

interface Weekly {
    days: [boolean, boolean, boolean, boolean, boolean, boolean, boolean] // Sun, Mon, Tue, Wed, Thr, Fri, Sat
    weeksInAdvance: number
}
