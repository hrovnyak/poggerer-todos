import { Accessor, For, Setter, children, createSignal, JSX, Show } from "solid-js"
import { AppInfo } from "./types"

function changeCategories(info: AppInfo, from: string | null, to: string | null) {
  let todos = info.todos()

  for (const todo of todos) {
    if (todo.category === from) {
      todo.category = to
    }
  }

  info.setTodos(todos)

  let generators = info.generators()

  for (const generator of generators) {
    if (generator.category === from) {
      generator.category = to
    }
  }

  info.setGenerators(generators)
}

function validateName(info: AppInfo, name: string): string | true {
  if (name === "") {
    return "The category name must not be empty"
  }

  if (info.categories().includes(name)) {
    return "Two categories can't have the same name"
  }

  return true
}

export default function Categories(props: { info: AppInfo }) {
  let info = props.info

  let [showModal, setShowModal] = createSignal(false)

  let [modalState, setModalState] = createSignal("main")

  let [modalCategory, setModalCategory] = createSignal("")

  let renameTo = ""
  let [renameError, setRenameError] = createSignal("")

  return (
    <div class="categories">
      <Modal show={showModal} setShow={setShowModal}>
        <Show when={modalState() == "main"}>
          <h2 style="margin: 0">Do stuff</h2>
          <button onclick={() => setModalState("rename")}>Rename</button>
          <button onclick={() => setModalState("delete")}>Delete</button>
        </Show>
        <Show when={modalState() == "rename"}>
          <input oninput={(e) => renameTo = e.target.value} placeholder="Rename to" />
          <button onclick={() => {
            let maybeValidationError = validateName(info, renameTo)

            if (maybeValidationError !== true) {
              setRenameError(maybeValidationError)
              return
            }

            info.setCategories(info.categories().map(v => v === modalCategory() ? renameTo : v))

            changeCategories(info, modalCategory(), renameTo)

            setShowModal(false)
          }}>Rename</button>
          <p>{renameError()}</p>
        </Show>
        <Show when={modalState() == "delete"}>
          <h2 style="margin: 0">Are you sure?</h2>
          <button onclick={() => {
            info.setCategories(info.categories().filter(v => v !== modalCategory()))

            changeCategories(info, modalCategory(), null)

            setShowModal(false)
          }}>Yes</button>
        </Show>
      </Modal>
      <h2 id="categories-label">Categories</h2>
      <For each={info.categories()}>
        {(category) =>
          <div class="category" onclick={() => {
            info.setCategory(category)
            info.setPage("todos")
          }} oncontextmenu={(e) => {
            e.preventDefault()
            setShowModal(true)
            setModalState("main")
            setModalCategory(category)
            renameTo = ""
            setRenameError("")
          }}>{category}</div>
        }
      </For>
    </div>
  )
}

function Modal(props: { show: Accessor<boolean>, setShow: Setter<boolean>, children: JSX.Element }) {
  const c = children(() => props.children)

  return <>
    <div class="modal-container" style={`visibility: ${props.show() ? "visible" : "hidden"}`} onclick={() => {
      props.setShow(false)
    }}>
      <div class="modal" onclick={e => e.stopPropagation()}>
        {c()}
      </div>
    </div>
  </>
}

