import { Accessor, ComponentProps, For, Setter, Show, createSignal } from 'solid-js'
import Categories from "./Categories"
import { AppInfo, Todo, Generator } from './types'

function App() {
  let [page, setPage] = createSignal<"categories" | "todos" | "generators">("categories")

  let [category, setCategory] = createSignal<string | null>(null)

  let [categories, setCategories] = createSignal(["poggers", "yeet", "oof", "yoyleberry pie"])

  let [todos, setTodos] = createSignal<Todo[]>([])

  let [generators, setGenerators] = createSignal<Generator[]>([])

  let info: AppInfo = {
    page,
    setPage,
    category,
    setCategory,
    categories,
    setCategories,
    todos,
    setTodos,
    generators,
    setGenerators,
  }

  return (
    <>
      <Show when={page() === "categories"}>
        <Categories info={info} />
      </Show>
    </>
  )
}

export default App
